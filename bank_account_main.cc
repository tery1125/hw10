#include <iostream>
#include <string>
#include "bank_account.h"
using namespace std;

int main() {
	vector<Account*> acs;
	string cmd;
	while (cmd != "quit") {
		cin >> cmd;
		if (cmd == "add") {
			string name;
			string mode;
			int bal;
			double rate;
			cin >> name >> mode >> bal >> rate;
			Account* a = CreateAccount(mode,name,bal,rate);
			acs.push_back(a);
		}
		else if (cmd == "delete") {
			string name;
			cin >> name;
			for(int i=0; i<acs.size(); i++) {
				if (acs[i]->name() == name)
				acs.erase(acs.begin()+i);
			}
		}
		else if (cmd == "show") {
			for(int i=0; i<acs.size(); i++) {
				cout << acs[i]->name() << " " << acs[i]->type() << " " <<
				acs[i]->balance() << " " << acs[i]->interest_rate() << endl;
			}
		}
		else if (cmd == "after") {
			int yrs;
			cin >> yrs;
			for(int i=0; i<acs.size(); i++) {
				cout << acs[i]->name() << " " << acs[i]->type() << " " <<
				acs[i]->ComputeExpectedBalance(yrs) << " " << acs[i]->interest_rate() << endl;
			}
		}
		else if (cmd == "save") {
			string file;
			cin >> file;
			SaveAccounts(acs, file);
		}
		else if (cmd == "load") {
			string file;
			cin >> file;
			LoadAccounts(file, &acs);
		}
	}
  return 0;
}
