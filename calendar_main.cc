#include <iostream>
#include <string>
#include "calendar.h"
using namespace std;

int main() {
  Date date; 
  string cmd;
  while (cmd != "quit") {
    cin >> cmd;
    try {
      if (cmd == "set") {
        cin >> date;
        cout << date << endl;
      } else if (cmd == "next") {
        int tick = 1;
        cin >> tick;
        date.NextDay(tick);
        cout << date << endl;
      }
	else if (cmd == "next_day") {
	date.NextDay(1);
        cout << date << endl;
      }
    } catch (InvalidDateException& e) {
      cout << "Invalid date: " << e.input_date << endl;
    }
  }
  return 0;
}
