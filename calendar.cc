#include <iostream>
#include "calendar.h"

using namespace std;
typedef struct InvalidDateException ids;
Date::Date()
{
	year_ = 0;
	month_ = 1;
	day_ = 1;
}

Date::Date(int year, int month, int day)
{
	year_ = year;
	month_ = month;
	day_ = day;
}

void Date::NextDay(int n)
{
	int a = ComputeDaysFromYearStart(year_, month_, day_) + n;
	while(a<0)
	{
		a+=GetDaysInYear(year_-1);
		year_--;
	}
	while(a>=GetDaysInYear(year_))
	{
		a-=GetDaysInYear(year_);
		year_++;
	}
        month_ = 1;
        day_ = 1;
	while(a>=daysInMonth(month_,year_))
	{
		a -= daysInMonth(month_,year_);
		month_++;
	}
        day_ += a;
}

bool Date::SetDate(int year, int month, int day)
{
	if(month>12||month<=0||day>daysInMonth(month,year)||day<0)
		return false;
	year_ = year;
	month_ = month;
	day_ = day;
	return true;
}

int Date::GetDaysInYear(int year)
{
	if(year%400 == 0)
		return 366;
	if(year%100 == 0)
		return 365;
	if(year%4 == 0)
		return 366;
	return 365;
}

 // 윤년을 판단하여 주어진 연도에 해당하는 날짜 수(365 또는 366)를 리턴.
int Date::daysInMonth(int month, int year)
{
	if(month == 1)
		return 31;
	if(month == 2)
	{	
		if(GetDaysInYear(year)==366)
			return 29;
		else
			return 28;
	}
	if(month == 3)
		return 31;
	if(month == 4)
		return 30;
	if(month == 5)
		return 31;
	if(month == 6)
		return 30;
	if(month == 7)
		return 31;
	if(month == 8)
		return 31;
	if(month == 9)
		return 30;
	if(month == 10)
		return 31;
	if(month == 11)
		return 30;
	if(month == 12)
		return 31;
	return 0;
}


 // 해당 날짜가 해당 연도의 처음(1월 1일)부터 며칠째인지를 계산.

int Date::ComputeDaysFromYearStart(int year, int month, int day)
{
	int answer=0;
	for(int i = 1; i< month; i++)
	{
		answer += daysInMonth(i, year);
	}
	answer += day;
	return answer - 1;
}

ostream& operator<<(ostream& os, const Date& c)
{
	os<<c.year()<<"."<<c.month()<<"."<<c.day();
	return os;
}

istream& operator>>(istream& is, Date& c)
{
	string str;
	is>>str;
	string syear;
	string smonth;
	string sday;
	int y,m,d;
	int i;	
	for(i = 0; i<str.size(); i++)
	{
		if(str[i] == '.')
			break;
		syear += str[i];
	}
	i++;
	for(; i<str.size(); i++)
	{
		if(str[i] == '.')
			break;
		smonth += str[i];
	}
	i++;
	for(; i<str.size(); i++)
	{
		if(str[i] == '.')
			break;
		sday += str[i];
	}
	y = atoi(syear.c_str());
	m = atoi(smonth.c_str());
	d = atoi(sday.c_str());
	if(c.SetDate(y,m,d)==false)
		throw ids(str);

	return is;
}
