#include <iostream>
#include <string>
#include "aus_vote.h"
using namespace std;
typedef vector<Candidate> RoundResult;

int main() {
	int cannum;
	cin >> cannum;
	vector<string> names;
	string name;
        names.reserve(cannum);
	for(int i=0; i < cannum; ++i) {
		cin >> name;
		names.push_back(name);
	}
	AusVoteSystem ausv(names);
	int votnum;
	cin >> votnum;
	for(int i=0; i<votnum; i++) {
		vector<int> vote;
		for(int i=0; i<cannum; i++) {
			int hubo;
			cin >> hubo;
			vote.push_back(hubo);
		}
		ausv.AddVote(vote);	
	}
	vector<RoundResult> res = ausv.ComputeResult();
	int k;
	for(int i = 0; i<res.size(); i++) {
		cout << "Round " << i+1 << ":";
		for(int j=0; j<res[i].size(); j++) {
			if(res[i][j].votes!=-1)
			cout << " " << res[i][j].name << " " << res[i][j].votes;
		}
		cout << endl;
		k=i;
	}
	string winner="";
	int max=0;
	for(int i=0 ; i < res[k].size() ; i++) {
		if( res[k][max].votes < res[k][i].votes )
			max=i;
	}
	
	if( res[k][max].votes > 0 )
		winner = res[k][max].name;
	for(int i=0 ; i < res[k].size() ; i++) {
		if( max == i ) continue;		
		if( res[k][max].votes == res[k][i].votes )
			winner="";
	}
	
	cout << "Winner: " << winner <<endl;
	return 0;
}
