#include "bank_account.h"

using namespace std;

Account::Account(const string& name, unsigned int balance, double interest_rate)
{
	name_ = name;
	balance_ = balance;
	interest_rate_ = interest_rate;
}
Account::~Account()
{}
void Account::Deposit(unsigned int amount)
{
	balance_ += amount;
}
bool Account::Withdraw(unsigned int amount)
{
	if(amount>balance_)
		return false;
	balance_ -= amount;
	return true;
}
unsigned int Account::ComputeExpectedBalance(unsigned int n_years_later) const
{
	return balance_ + balance_ * interest_rate_ * n_years_later;
}
SavingAccount::SavingAccount(const string& name, int balance, double interest_rate) : Account(name, balance, interest_rate)
{}
SavingAccount::~SavingAccount()
{}
// 이 타입의 계좌는 복리로 계산.
unsigned int SavingAccount::ComputeExpectedBalance(unsigned int n_years_later) const
{
	double answer = balance_;
	for(int i=0;i<n_years_later;i++)
	{
		answer *= 1+interest_rate_;
	}
	return int(answer);
}

Account* CreateAccount(const string& type, const string& name, unsigned int balance, double interest_rate)
{	
	if(type=="checking") {
		Account* a = new Account(name,balance,interest_rate);
		return a;
	}
	if(type=="saving")
	{
		Account* a = new SavingAccount(name,balance,interest_rate);
		return a;
	}
}
      
bool SaveAccounts(const vector<Account*>& accounts, const string& filename)
{
	ofstream fout;
	fout.open(filename.c_str());
	if(fout.is_open() == false)
		return false;
	for(int i=0;i<accounts.size();i++) {
		fout<<accounts[i]->name()<<" "<<accounts[i]->type()<<" "<<accounts[i]->balance()<<" "<<accounts[i]->interest_rate()<<endl;
	}
	fout.close();
	return true;
}

bool LoadAccounts(const string& filename, vector<Account*>* accounts)
{
	string name;
	string type;
	int bal;
	double rate;
	ifstream fin;
	fin.open(filename.c_str());
	if(fin.is_open()==false)
		return false;
	while(1) {
		fin>>name>>type>>bal>>rate;
		if(fin.eof()) {break;}
		Account* a = CreateAccount(type,name,bal,rate);
		accounts->push_back(a);
	}
	fin.close();
	return true;
}




