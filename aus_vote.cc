#include "aus_vote.h"
	
using namespace std;

AusVoteSystem::AusVoteSystem(const vector<string>& candidate_names)
{
	for(int i=0;i<candidate_names.size();i++) {
		Candidate a;
		a.name = candidate_names[i];
		a.votes = 0;
		names_.push_back(a);
	}
}
// 후보별 선호도를 1순위부터 입력.
// 잘못된 숫자가 있거나 선호도 수가 후보자 수와 다르면
// 해당 입력을 무시하고 false를 리턴.
bool AusVoteSystem::AddVote(const vector<int>& vote)
{
	for(int i=0;i<vote.size();i++) {
		for(int j=i+1;j<vote.size();j++) {
			if(vote[i] == vote[j])
				return false;
		}
	}
	votes_.push_back(vote);
	return true;	
}
typedef vector<Candidate> RoundResult;
// 지금까지의 투표를 바탕으로 결과를 모든 라운드에 대해 계산.
// main() 에서는 vector<RoundResult>를 출력형식s에 맞도록 출력.
vector<RoundResult> AusVoteSystem::ComputeResult()
{
	vector<RoundResult> answer;
	RoundResult names2_ = names_;
	for(int ik=0; ik<names_.size(); ik++)
	{
		for(int i=0; i<names_.size(); i++) {
			if(names_[i].votes!=-1)
				names_[i].votes=0;
		}
		for(int i=0; i<votes_.size(); i++) {
			names_[votes_[i][0]-1].votes++;
		}
		answer.push_back(names_);
		int min = names_[0].votes;
		for(int i=0; i<names_.size(); i++) {
			if(names_[i].votes<min && names_[i].votes>-1)
				min = names_[i].votes;
		}
		int max = names_[0].votes;
		for(int i=0; i<names_.size(); i++) {
			if(names_[i].votes>max)
				max = names_[i].votes;
		}
		if(max>votes_.size()/2)
			break;
		for(int i=0; i<names_.size(); i++) {
			if(names_[i].votes==min) {
				names_[i].votes=-1;
				for(int j=0; j<votes_.size(); j++) {
					for(int k=0; k<votes_[j].size(); k++) {
						if(votes_[j][k]==i+1)
							votes_[j].erase(votes_[j].begin()+k);
					}
				}
			}
		}
		if(votes_[0].size()==0)
			break;
	}
	return answer;
}





















